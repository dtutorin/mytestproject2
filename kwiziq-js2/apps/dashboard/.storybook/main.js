module.exports = {
  "stories": [
    "../../../libs/components/src/lib/**/*.stories.js",
    "../../../libs/ui/src/lib/**/*.stories.js",
  ],
  "addons": [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-interactions",
    "@storybook/addon-postcss",
  ],
  "webpackFinal": async config => {
    config.module.rules.push(
        {
          test: /\.ts$/,
          loader: "ts-loader",
          options: { appendTsSuffixTo: [/\.vue$/] },
        },
        {
          test: /\.scss$/,
          use: ['style-loader', 'css-loader', 'sass-loader'],
        },
        {
          test: /\.css$/,
          use: ['style-loader', 'css-loader'],
        },
      );
    return config;
  },
  "framework": "@storybook/vue3",
  "core": {
    "builder": "webpack5", //This will tell Storybook to use Webpack 5
  },
  "previewHead": (head) => (`
    ${head}
    <style>
      @import url('https://french.kwiziq.vubuntu.spottmedia.co.uk/css/src/CssLiteSite.4018583f.css');
      @import url('https://french.kwiziq.vubuntu.spottmedia.co.uk/css/src/CssSite.4a13ac65.css');
    </style>
  `),
}