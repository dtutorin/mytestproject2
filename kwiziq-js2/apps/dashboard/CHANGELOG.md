# Changelog

This file was generated using [@jscutlery/semver](https://github.com/jscutlery/semver).

### [0.0.2](https://gitlab.com/dtutorin/mytestproject2/compare/dashboard@0.0.1...dashboard@0.0.2) (2022-11-11)

### 0.0.1 (2022-11-11)

## [1.2.0](https://gitlab.com/kwiziq/projects/apps/kwiziq-js/compare/dashboard@1.1.0...dashboard@1.2.0) (2022-11-11)

## [1.1.0](https://gitlab.com/kwiziq/projects/apps/kwiziq-js/compare/dashboard@1.0.0...dashboard@1.1.0) (2022-11-11)

## [1.0.0](https://gitlab.com/kwiziq/projects/apps/kwiziq-js/compare/dashboard@0.1.0...dashboard@1.0.0) (2022-11-11)

## [0.1.0](https://gitlab.com/kwiziq/projects/apps/kwiziq-js/compare/dashboard@0.0.2...dashboard@0.1.0) (2022-11-11)

### [0.0.2](https://gitlab.com/kwiziq/projects/apps/kwiziq-js/compare/dashboard@0.0.1...dashboard@0.0.2) (2022-11-11)

### 0.0.1 (2022-11-11)
