/*
 * Modify the webpack config by exporting an Object or Function.
 *
 * If the value is an Object, it will be merged into the final
 * config using `webpack-merge`.
 *
 * If the value is a function, it will receive the resolved config
 * as the argument. The function can either mutate the config andmodule.exports = (config) => {
 * return nothing, OR return a cloned or merged version of the config.
 *
 * https://cli.vuejs.org/config/#configurewebpack
 */
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const LAUNCH_COMMAND = process.env.npm_lifecycle_event;
const PACKAGE_NAME = process.env.LERNA_PACKAGE_NAME;
const isProduction = LAUNCH_COMMAND === 'prod';
const PACKAGE = require('./package.json');
const VERSION = PACKAGE.version;


module.exports = {
  output: {
    filename: `${PACKAGE_NAME}_${VERSION}_[hash].js`,
  },
  optimization: {
    minimize: true,
    minimizer: [new UglifyJsPlugin({
      uglifyOptions: {
        warnings: false,
        parse: {},
        compress: {},
        output: null,
        toplevel: false,
        nameCache: null,
        ie8: false,
        keep_fnames: false,
      },
    }),],
    splitChunks: false,
  },
  plugins: [
    new BundleAnalyzerPlugin({
        openAnalyzer: true,
    }),
  ]
}
