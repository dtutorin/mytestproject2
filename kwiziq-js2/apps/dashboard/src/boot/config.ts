import { useStore } from '../stores';
import * as devConfig from "../config/config.dev.json";
import * as prodConfig from "../config/config.uat.json";


export const bootApi = async () => {
  const store = useStore();
  let baseApiUrl = '';
  if (process.env.NODE_ENV === 'development') {
    baseApiUrl = devConfig.VUE_APP_SERVER_URI;
    store.api.setBaseAPI(baseApiUrl);
  }
  return console.log(baseApiUrl);
}