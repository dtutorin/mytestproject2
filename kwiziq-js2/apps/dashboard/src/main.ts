import { createApp } from 'vue';
import { createPinia } from 'pinia';
import App from './App.vue';
import router from './router';
import { bootApi } from './boot/config';

const pinia = createPinia();
const app = createApp(App);

const boot = () => {
  return bootApi();
};

app.use(router).use(pinia).use(boot).mount('#dashboard');
