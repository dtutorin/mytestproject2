import { ApiRepository } from '../data/api';

export class QwizService {
  constructor(
    public api: ApiRepository,
  ) {}
  async getAnnouncements(): Promise< { data: any } | undefined> {
    const response = await this.api.kwiz.list();
    if (response?.data) {
      const data = response?.data;
      return {data};
    }
  }
}
