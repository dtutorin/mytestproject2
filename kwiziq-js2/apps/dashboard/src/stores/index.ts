import { defineStore } from 'pinia';
import progressStore from './modules/progress';
import apiStore from './modules/api';

export const useStore = defineStore('index', () => {
  const progress = progressStore();
  const api = apiStore();
  return {
    progress,
    api,
  }
});
