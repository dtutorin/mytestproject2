import {defineStore} from 'pinia';

// import { IAwardsItemData } from '@kwiziq/data'

export const useApiStore = defineStore('api', {
  state: () => ({
    baseAPI: '',
  }),

  getters: {
    getBaseAPI: (state) => state.baseAPI,
  },

  actions: {
    setBaseAPI(api: string) {
      this.baseAPI = api;
    },
  }
});

export default useApiStore;
