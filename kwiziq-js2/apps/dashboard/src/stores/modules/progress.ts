import {defineStore} from 'pinia';
import axios from 'axios';
import useApiStore from './api';

// import  {IConfidenceData } from '@kwiziq/data';

export const useProgressStore = defineStore('progress', {
  state: () => ({
    userLevel: 0,
    confidence: {},
    todayTaken: {},
    todayAwards: {},

  }),
  getters: {
    getUserLevel: (state) => state.userLevel,
    getConfidence: (state) => state.confidence,
    getTodayTaken: (state) => state.todayTaken,
    getTodayAwards: (state) => state.todayAwards,
  },
  actions: {
    async fetchUserLvl() {
      try {
        const response = await axios({
          method: 'get',
          url: 'https://french.kwiziq.vubuntu.spottmedia.co.uk/api/users/3/achievements/recommended-level',
        });
        if (response.data) {
          console.log(response.data)
          this.userLevel = response.data.data;
        }
      } catch (e) {
        console.log(e);
      }
    },
    async fetchUserConfidence() {
      const baseURL = useApiStore().getBaseAPI;
      try {
        const response = await axios({
          method: 'get',
          url: `${baseURL}api/users/80926/achievements/level-summary`,
        });
        if (response.data) {
          console.log(response.data);
          this.confidence = response.data.data;
        }
      } catch (e) {
        console.log(e);
      }
    },
    async fetchUserStatistics() {
      const baseURL = useApiStore().getBaseAPI;
      try {
        const response = await axios({
          method: 'get',
          url: `${baseURL}api/users/80926/achievements/user-statistics`,
        });
        if (response.data) {
          console.log(response.data);
          this.todayTaken = response.data.data;
        }
      } catch (e) {
        console.log(e);
      }
    },
    async fetchUserAwards() {
      const baseURL = useApiStore().getBaseAPI;
      try {
        const response = await axios({
          method: 'get',
          url: `${baseURL}api/users/80926/achievements/awards/today`,
        });
        if (response.data) {
          console.log(response.data);
          this.todayAwards = response.data;
        }
      } catch (e) {
        console.log(e);
      }
    },
  }
});

export default useProgressStore;