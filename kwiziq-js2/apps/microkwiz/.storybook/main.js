module.exports = {
  "stories": ['../src/components/**/*.stories.js'],
  "addons": [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-interactions",
    "@storybook/addon-postcss",
  ],
  "webpackFinal": async config => {
    config.module.rules.push(
        {
          test: /\.ts$/,
          loader: "ts-loader",
          options: { appendTsSuffixTo: [/\.vue$/] },
        },
        {
          test: /\.scss$/,
          use: ['style-loader', 'css-loader', 'sass-loader'],
        },
        {
          test: /\.css$/,
          use: ['style-loader', 'css-loader'],
        },
      );
    return config;
  },
  "framework": "@storybook/vue3",
  "core": {
    "builder": "webpack5", //This will tell Storybook to use Webpack 5
  },
}