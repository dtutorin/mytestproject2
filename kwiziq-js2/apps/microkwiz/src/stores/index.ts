import { defineStore } from 'pinia';
import kwizStore from './modules/kwiz';

export const useStore = defineStore('index', () => {
  const kwiz = kwizStore();
  return {
    kwiz,
  }
})