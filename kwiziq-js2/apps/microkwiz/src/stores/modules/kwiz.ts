import { defineStore } from 'pinia';
import { IConfigData, IKwizTest, IKwizTestQuestion, IDataMessage, } from '../../interfaces';
import axios from 'axios';

export default defineStore('counter', {
  state: () => ({
    configData: {} as IConfigData,
    questionNumber: 0,
    kwizTest: {} as IKwizTest,
    tests: [] as IKwizTestQuestion[],
    message: {} as IDataMessage,
  }),
  getters: {
    getTitle: (state) => state.configData.returnTitle,
    getTest: (state) => state.kwizTest,
    getTests: (state) => state.tests,
    getResultURL: (state) => state.kwizTest.results_url,
    getQuestionNumber: (state) => state.questionNumber,
    getMessage: (state) => state.message,
  },
  actions: {
    setupConfig(data: IConfigData) {
      this.configData = data;
    },

    errorHandler(err: any){
      const defaultErrorMessage = 'Sorry, gremlins in the system have prevented the Kwiz from loading please try refreshing the page.';
      this.message = {
        text: defaultErrorMessage,
      };
      console.log(err);
    },

    async fetchQwiz() {
      const timeStamp = Math.floor(Date.now() / 1000);
      const returnTitle = this.configData.returnTitle;
      const returnUrl = this.configData.returnURL;
      const qwizId = this.configData.kwizKey;
      const currentUrl = this.configData.url;
      try {
        const response = await axios({
          method: 'get',
          url: `${currentUrl}/kwizzes/view/${qwizId}.json?kqspercomp=&src_url=${encodeURIComponent(returnUrl)}&src_title=${encodeURIComponent(returnTitle)}&ts=${timeStamp}`
        });
        if (response.status === 200 && response?.data) {
          this.message = {};
          if (response?.data?.Kwiz?.Message) {
            this.message = response.data.Kwiz.Message;
            return;
          }
          this.kwizTest = response.data.Kwiz.Test;
          this.tests.push(...response.data.Kwiz.TestsTestquestion);
        }
      } catch (e) {
        if(e) {
          this.errorHandler(e)
        };
      }
    },
    increaseQuestionNumber() {
      ++this.questionNumber;
    },
  }
})