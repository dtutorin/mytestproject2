module.exports = {
  displayName: 'components',
  preset: '../../jest.preset.js',
  transform: {
    '^.+.vue$': 'vue3-jest',
    '.+.(css|styl|less|sass|scss|svg|png|jpg|ttf|woff|woff2)$':
      'jest-transform-stub',
    '^.+.tsx?$': 'ts-jest',
  },
  moduleFileExtensions: ['ts', 'tsx', 'vue', 'js', 'json'],
  coverageDirectory: '../../coverage/libs/components',
  snapshotSerializers: ['jest-serializer-vue'],
  globals: {
    'ts-jest': {
      tsconfig: 'libs/components/tsconfig.spec.json',
      babelConfig: 'libs/components/babel.config.js',
    },
    'vue-jest': {
      tsConfig: 'libs/components/tsconfig.spec.json',
      babelConfig: 'libs/components/babel.config.js',
    },
  },
};
