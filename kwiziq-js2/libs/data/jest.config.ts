module.exports = {
  displayName: 'data',
  preset: '../../jest.preset.js',
  transform: {
    '^.+.vue$': 'vue3-jest',
    '.+.(css|styl|less|sass|scss|svg|png|jpg|ttf|woff|woff2)$':
      'jest-transform-stub',
    '^.+.tsx?$': 'ts-jest',
  },
  moduleFileExtensions: ['ts', 'tsx', 'vue', 'js', 'json'],
  coverageDirectory: '../../coverage/libs/data',
  snapshotSerializers: ['jest-serializer-vue'],
  globals: {
    'ts-jest': {
      tsconfig: 'libs/data/tsconfig.spec.json',
      babelConfig: 'libs/data/babel.config.js',
    },
    'vue-jest': {
      tsConfig: 'libs/data/tsconfig.spec.json',
      babelConfig: 'libs/data/babel.config.js',
    },
  },
};
