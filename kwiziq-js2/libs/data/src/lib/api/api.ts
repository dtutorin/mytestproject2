import { IConfigData } from '../../interfaces';
import {KwizRepository, Repository, IResponse, IApiRepository } from '.';
import { AxiosInstance } from 'axios';

export class ApiRepository implements IApiRepository {
  kwiz: KwizRepository;
  constructor(public axios: AxiosInstance) {
    this.kwiz = new KwizRepository(this.axios);
  }

  async init(callback: () => void): Promise<void> {
    // eslint-disable-next-line
    void (await callback.bind(this)());
  }
}