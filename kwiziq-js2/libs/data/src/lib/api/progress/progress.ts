import { Repository, IResponse } from '..';
import { IConfigData } from '../../../interfaces/index';

export class KwizRepository extends Repository {
  async list(): Promise<IResponse<[]> | undefined> {
    const timeStamCssLiteSite.14772421.cssp = Math.floor(Date.now() / 1000);
    const returnTitle = 'Test your knowledge with Kwiziq and Learn French with Alexa'
    const returnUrl = 'https://progress.lawlessfrench.com/revision/grammar/conjugate-avoir-jai-tu-as-vous-avez-in-le-present-present-tense';
    const response = await this.request<[]>({
      method: 'get',
      path: `https://french.kwiziq.vubuntu.spottmedia.co.uk/api/kwizzes/view/3146.json?kqspercomp=&src_url=${encodeURIComponent(returnUrl)}&src_title=${encodeURIComponent(returnTitle)}&ts=${timeStamp}`,
    });
    return response;
  }
}