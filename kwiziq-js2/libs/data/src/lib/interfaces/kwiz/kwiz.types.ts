export interface IConfigData {
  url: string;
  returnURL: string;
  returnTitle: string;
  title?: string;
  token: string;
  trkCat?: string;
  questionsPerComp?: string;
  kwiziqContext?: string;
  kwizKey: string;
  status?: string;
  iframeResults?: boolean;
}

export interface ITestsResponse {
  Kwiz: ITests;
}

export interface ITests {
  Test: IKwizTest;
  TestsTestquestion: IKwizTestQuestion[];
}

export interface IKwizTest {
  application_id: number | null;
  attachment_id: number | null;
  created: string;
  description: string;
  id: string;
  language_1_id: string;
  language_2_id: string;
  lock_issued: number| null;
  locked_by: number | null;
  modified: string
  name: string;
  results_url: string;
  site_url: string;
  status: string;
  studylist_id: string;
  sub_type: string;
  type: string;
  user_id: string;
  voice_default: number | null;
}

export interface IKwizTestQuestion {
  Testquestion: ITestQuestion;
  id: string;
  test_id: string;
  testquestion_id: string;
}

export interface ITestQuestion {
  Testanswer: IKwizAnswer[];
  hint: string;
  id: string;
  skill_dimension_id: string;
  testquestion_type_id: string;
  text: string;
}

export interface IKwizAnswer {
  id: string;
  testquestion_id: string;
  text: string;
}