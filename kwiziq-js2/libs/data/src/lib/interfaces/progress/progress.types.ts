export interface IUserLevelData {
  data: string;
}

export interface IConfidenceData {
  data: [
    {
      A0: string;
      diff: number
    },
    {
      A1: string;
      diff: number
    },
    {
      A2: string;
      diff: number
    },
    {
      B2: string;
      diff: number
    },
    {
      C1: string;
      diff: number
    }
  ]
}

export interface IUserStatisticData {
  data: {
    takesToday: number;
    takesThisMonth: number;
    totalTakes: number;
    dailyGoal: number;
    allQuestionsCompleted: number;
  }
}

export interface IAwardsData {
  data: IAwardsItemData [];
}

export interface IAwardsItemData {
    title: string;
    awardedFor: string;
    entityName: string;
    reward: string;
    flavour: string;
    kudosPoints: string;
    achievementId: string;
    achievementTypeId: string;
    entityId: string;
    achievementType: string;
    entity: string;
    slug: string;
    achieved_dt: string;
}