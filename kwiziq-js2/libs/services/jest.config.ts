module.exports = {
  displayName: 'services',
  preset: '../../jest.preset.js',
  transform: {
    '^.+.vue$': 'vue3-jest',
    '.+.(css|styl|less|sass|scss|svg|png|jpg|ttf|woff|woff2)$':
      'jest-transform-stub',
    '^.+.tsx?$': 'ts-jest',
  },
  moduleFileExtensions: ['ts', 'tsx', 'vue', 'js', 'json'],
  coverageDirectory: '../../coverage/libs/services',
  snapshotSerializers: ['jest-serializer-vue'],
  globals: {
    'ts-jest': {
      tsconfig: 'libs/services/tsconfig.spec.json',
      babelConfig: 'libs/services/babel.config.js',
    },
    'vue-jest': {
      tsConfig: 'libs/services/tsconfig.spec.json',
      babelConfig: 'libs/services/babel.config.js',
    },
  },
};
