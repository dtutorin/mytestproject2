module.exports = {
  displayName: 'ui',
  preset: '../../jest.preset.js',
  transform: {
    '^.+.vue$': 'vue3-jest',
    '.+.(css|styl|less|sass|scss|svg|png|jpg|ttf|woff|woff2)$':
      'jest-transform-stub',
    '^.+.tsx?$': 'ts-jest',
  },
  moduleFileExtensions: ['ts', 'tsx', 'vue', 'js', 'json'],
  coverageDirectory: '../../coverage/libs/ui',
  snapshotSerializers: ['jest-serializer-vue'],
  globals: {
    'ts-jest': {
      tsconfig: 'libs/ui/tsconfig.spec.json',
      babelConfig: 'libs/ui/babel.config.js',
    },
    'vue-jest': {
      tsConfig: 'libs/ui/tsconfig.spec.json',
      babelConfig: 'libs/ui/babel.config.js',
    },
  },
};
