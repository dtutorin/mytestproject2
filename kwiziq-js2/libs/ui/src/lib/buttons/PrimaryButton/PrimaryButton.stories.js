import { action } from '@storybook/addon-actions';

import PrimaryButton from './PrimaryButton.vue';

export default {
  title: 'Brand Primary Button',
  component: PrimaryButton,
};

export const BrandPrimaryIconLeft = () => ({
  components: { PrimaryButton },
  template: `
    <PrimaryButton @click="action">
    <span slot="left">
    «
    </span>
    Previous
      
    </PrimaryButton>
  `,
  methods: { action: action('clicked') },
});
export const BrandPrimaryIconRight = () => ({
  components: { PrimaryButton },
  template: `
    <PrimaryButton @click="action">
      Next
      <span slot="right">
      »
      </span>
    </PrimaryButton>
  `,

  methods: { action: action('clicked') },
});

export const BrandPrimaryIconLeftDisabled = () => ({
  components: { PrimaryButton },
  template: `
    <PrimaryButton @click="action" disabled>
      <span slot="left">
        «
      </span>
      Previous
      
    </PrimaryButton>
  `,
  methods: { action: action('clicked') },
});
export const BrandPrimaryIconRightDisabled = () => ({
  components: { PrimaryButton },
  template: `
    <PrimaryButton @click="action" disabled>
      Next
      <span slot="right">
      »
      </span>
    </PrimaryButton>
  `,

  methods: { action: action('clicked') },
});
