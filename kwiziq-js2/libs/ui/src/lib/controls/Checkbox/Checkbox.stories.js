import Checkbox from './Checkbox.vue';

export default {
  title: 'Checkbox',
  component: Checkbox,
  argTypes: {
    disabled: { control: 'boolean' },
    value: { control: 'boolean' },
    bold: { control: 'boolean' },
    white: { control: 'boolean' },
  },
  args: {
    value: false,
    disabled: false,
    bold: false,
    white: false,
  },
};

export const Normal = (args, { argTypes }) => ({
  components: { Checkbox },
  props: Object.keys(argTypes),
  template: `
  <div :style="{
    'background-color': white ? '#478DC8' : '#fff',
    padding: '25px'
  }">
    <Checkbox v-model="value" :white="white" :disabled="disabled" :bold="bold">
      Label
    </Checkbox>
  </div>
  `,
});
