import PanelAwardsToday from './PanelAwardsToday.vue';

export default {
  title: 'Pannel Awards Today',
  component: PanelAwardsToday,
  argTypes: {
    iconStarType: {control: { type: 'select', options: ['bronze', 'silver', 'gold', 'diamond']}},
  },
};

const TemplateLight = (args, {argTypes}) => ({
  components: { PanelAwardsToday },
  props: Object.keys(argTypes),
  setup() {
    return { args };
  },
  template: `
    <div class="theme--light">
      <PanelAwardsToday
      v-bind="args"
      />
    </div>
    `,

});

const TemplateDark = (args, {argTypes}) => ({
  components: { PanelAwardsToday },
  props: Object.keys(argTypes),
  setup() {
    return { args };
  },
  template: `
    <div class="theme--dark">
      <PanelAwardsToday
        v-bind="args"
      />
    </div>
    `,

});

export const LightAwards = TemplateLight.bind({});

LightAwards.args = {
  iconStarType: "bronze",
};

export const DarkAwards = TemplateDark.bind({});
LightAwards.args = {
  iconStarType: "bronze",
};