import PanelConfidence from './PanelConfidence.vue';

export default {
  title: 'Panel Level and Confidence',
  component: PanelConfidence,
  argTypes: {
    level: {control: { type: 'select', options: ['A0', 'A1', 'A2', 'B1', 'B2', 'C1']}},
  },
};

const TemplateLight = (args, {argTypes}) => ({
  components: { PanelConfidence },
  props: Object.keys(argTypes),
  setup() {
    return { args };
  },
  template: `
    <div class="theme--light">
      <PanelConfidence
        v-bind="args"
      />
    </div>
    `,

});

const TemplateDark = (args, {argTypes} ) => ({
  components: { PanelConfidence },
  props: Object.keys(argTypes),
  setup() {
    return { args };
  },
  template: `
    <div class="theme--dark">
      <PanelConfidence
        v-bind="args"
      />
    </div>
    `,

});

export const LevelAndConfidenceLight = TemplateLight.bind({});

LevelAndConfidenceLight.args = {
  level: 'A2',
  confidence: 100,
  diff: -100,
};

export const LevelAndConfidenceDark = TemplateDark.bind({});
LevelAndConfidenceDark.args = {
  level: 'A2',
  confidence: 100,
  diff: -100,

};
