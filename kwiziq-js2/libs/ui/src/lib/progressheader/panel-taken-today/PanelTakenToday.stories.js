import PanelTakenToday from './PanelTakenToday.vue';

export default {
  title: 'Panel Taken Today',
  component: PanelTakenToday,
  argTypes: {
    percentage: {control: { type: 'number'}},
  },
};

const TemplateLight = (args, {argTypes}) => ({
  components: { PanelTakenToday },
  props: Object.keys(argTypes),
  setup() {
    return { args };
  },
  template: `
    <div class="theme--light">
      <PanelTakenToday
        v-bind="args"
      />
    </div>
    `,

});

const TemplateDark = (args, {argTypes}) => ({
  components: { PanelTakenToday },
  props: Object.keys(argTypes),
  setup() {
    return { args };
  },
  template: `
    <div class="theme--dark">
      <PanelTakenToday
        v-bind="args"
      />
    </div>
    `,

});

export const LightToday = TemplateLight.bind({});

LightToday.args = {
  percentage: "60",
};

export const DarkToday = TemplateDark.bind({});
DarkToday.args = {
  percentage: "100",
};


// export const LightAwards = TemplateLight.bind({});
// LightAwards.args = {
//   title: 'Awards',
//   isToday: false,
// };

// export const DarkAwards = TemplateDark.bind({});
// DarkAwards.args = {
//   title: 'Awards',
//   isToday: false,
// };